
�"
openxc.protoopenxc"�
VehicleMessage/
type (2.openxc.VehicleMessage.TypeRtype3
can_message (2.openxc.CanMessageR
canMessage<
simple_message (2.openxc.SimpleMessageRsimpleMessageK
diagnostic_response (2.openxc.DiagnosticResponseRdiagnosticResponse?
control_command (2.openxc.ControlCommandRcontrolCommandB
command_response (2.openxc.CommandResponseRcommandResponse
	timestamp (R	timestamp"b
Type

UNUSED 
CAN

SIMPLE

DIAGNOSTIC
CONTROL_COMMAND
COMMAND_RESPONSE"�

CanMessage
bus (Rbus
id (Rid
data (RdataA
frame_format (2.openxc.CanMessage.FrameFormatRframeFormat"5
FrameFormat

UNUSED 
STANDARD
EXTENDED"�
ControlCommand/
type (2.openxc.ControlCommand.TypeRtypeO
diagnostic_request (2 .openxc.DiagnosticControlCommandRdiagnosticRequest_
passthrough_mode_request (2%.openxc.PassthroughModeControlCommandRpassthroughModeRequestn
 acceptance_filter_bypass_command (2%.openxc.AcceptanceFilterBypassCommandRacceptanceFilterBypassCommandR
payload_format_command (2.openxc.PayloadFormatCommandRpayloadFormatCommandn
 predefined_obd2_requests_command (2%.openxc.PredefinedObd2RequestsCommandRpredefinedObd2RequestsCommanda
modem_configuration_command (2!.openxc.ModemConfigurationCommandRmodemConfigurationCommand[
rtc_configuration_command (2.openxc.RTCConfigurationCommandRrtcConfigurationCommand"�
Type

UNUSED 
VERSION
	DEVICE_ID

DIAGNOSTIC
PASSTHROUGH
ACCEPTANCE_FILTER_BYPASS
PAYLOAD_FORMAT
PREDEFINED_OBD2_REQUESTS
MODEM_CONFIGURATION
RTC_CONFIGURATION	
SD_MOUNT_STATUS

PLATFORM
GET_VIN"�
DiagnosticControlCommand3
request (2.openxc.DiagnosticRequestRrequest?
action (2'.openxc.DiagnosticControlCommand.ActionRaction")
Action

UNUSED 
ADD

CANCEL"K
PassthroughModeControlCommand
bus (Rbus
enabled (Renabled"I
AcceptanceFilterBypassCommand
bus (Rbus
bypass (Rbypass"�
PayloadFormatCommandB
format (2*.openxc.PayloadFormatCommand.PayloadFormatRformat"3
PayloadFormat

UNUSED 
JSON
PROTOBUF"9
PredefinedObd2RequestsCommand
enabled (Renabled"�
NetworkOperatorSettings*
allowDataRoaming (RallowDataRoamingb
operatorSelectMode (22.openxc.NetworkOperatorSettings.OperatorSelectModeRoperatorSelectMode_
networkDescriptor (21.openxc.NetworkOperatorSettings.NetworkDescriptorRnetworkDescriptor�
NetworkDescriptor
PLMN (RPLMN_
networkType (2=.openxc.NetworkOperatorSettings.NetworkDescriptor.NetworkTypeRnetworkType"!
NetworkType
GSM 	
UTRAN"c
OperatorSelectMode
	AUTOMATIC 

MANUAL

DEREGISTER
SET_ONLY
MANUAL_AUTOMATIC"'
NetworkDataSettings
apn (	Rapn"?
ServerConnectSettings
host (	Rhost
port (Rport"�
ModemConfigurationCommandY
networkOperatorSettings (2.openxc.NetworkOperatorSettingsRnetworkOperatorSettingsM
networkDataSettings (2.openxc.NetworkDataSettingsRnetworkDataSettingsS
serverConnectSettings (2.openxc.ServerConnectSettingsRserverConnectSettings"6
RTCConfigurationCommand
	unix_time (RunixTime"t
CommandResponse/
type (2.openxc.ControlCommand.TypeRtype
message (	Rmessage
status (Rstatus"�
DiagnosticRequest
bus (Rbus

message_id (R	messageId
mode (Rmode
pid (Rpid
payload (Rpayload-
multiple_responses (RmultipleResponses
	frequency (R	frequency
name (	RnameH
decoded_type	 (2%.openxc.DiagnosticRequest.DecodedTypeRdecodedType"-
DecodedType

UNUSED 
NONE
OBD2"�
DiagnosticResponse
bus (Rbus

message_id (R	messageId
mode (Rmode
pid (Rpid
success (Rsuccess4
negative_response_code (RnegativeResponseCode
payload (Rpayload*
value (2.openxc.DynamicFieldRvalue
frame	 (Rframe

total_size
 (R	totalSize"�
DynamicField-
type (2.openxc.DynamicField.TypeRtype!
string_value (	RstringValue#
numeric_value (RnumericValue#
boolean_value (RbooleanValue"1
Type

UNUSED 

STRING
NUM
BOOL"{
SimpleMessage
name (	Rname*
value (2.openxc.DynamicFieldRvalue*
event (2.openxc.DynamicFieldReventB

com.openxcBBinaryMessagesbproto3